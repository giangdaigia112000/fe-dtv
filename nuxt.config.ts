// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    app: {
        head: {
            link: [
                {
                    rel: 'icon',
                    type: 'image/x-icon',
                    href: 'https://cdn.dienthoaivui.com.vn/wp-content/uploads/2019/03/cropped-Dien-thoai-Vui-favico-192x192-100x100.jpg',
                },
            ],
        },
    },
    ssr: true,
    srcDir: 'src/',
    imports: {
        dirs: ['stores'],
    },
    // ---------------------------------------------------------------- config env
    runtimeConfig: {
        test: process.env.TEST_ENV,
        public: {
            testclient: process.env.TEST_ENV,
        },
    },
    // ---------------------------------------------------------------- list Module
    modules: [
        '@nuxtjs/eslint-module',
        '@nuxt/image-edge',
        '@nuxtjs/tailwindcss',
        'nuxt-icon',
        'nuxt-swiper',
        [
            '@pinia/nuxt',
            {
                autoImports: ['defineStore', 'acceptHMRUpdate'],
            },
        ],
    ],

    // ---------------------------------------------------------------- css global
    css: ['@/assets/css/global.css', '@/assets/scss/main.scss'],

    // ---------------------------------------------------------------- tailwindcss config
    tailwindcss: {
        cssPath: '@/assets/css/tailwind.css',
    },
    swiper: {
        styleLang: 'scss',
    },
});
