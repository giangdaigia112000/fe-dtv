interface Data {
    id: number;
    code: string;
    content: string;
    name: string;
}

export const useStoreJsonServer = defineStore('jsonserver', {
    state: (): { banner: Data } => ({
        banner: {} as Data,
    }),

    getters: {
        getData: (state) => {
            const data = checkJson(state.banner.content);
            if (data) return data;
            return {};
        },
    },

    actions: {
        async getDataJsonServerGraphql(idJson: string) {
            const url = `https://api.stag.cps.onl/graphql-dashboard/graphql/query`;
            const axiosConfig = {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                timeout: 5000,
            };
            const body = JSON.stringify({
                query: `query JSON_SERVER {
                      cps_json(filter: {code: "${idJson}"})
                    }`,
                variables: {},
            });
            try {
                const result: any = await $fetch(url, {
                    method: 'post',
                    ...axiosConfig,
                    body,
                });
                this.$patch((state) => {
                    state.banner = result.data.cps_json;
                });
            } catch (error) {
                throw new Error('Get data false');
            }
        },
    },
});

const checkJson = (json: string) => {
    let jsonParse;
    try {
        jsonParse = JSON.parse(json);
    } catch (e) {
        return false;
    }
    return jsonParse;
};

if (import.meta.hot) {
    import.meta.hot.accept(
        acceptHMRUpdate(useStoreJsonServer, import.meta.hot)
    );
}
