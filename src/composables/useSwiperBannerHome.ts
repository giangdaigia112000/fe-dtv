import { Swiper } from 'swiper';

export default function () {
    const swiperTitle = new Swiper('.title-swiper', {
        breakpoints: {
            320: {
                slidesPerView: 3,
            },
            820: {
                slidesPerView: 3,
            },
            1024: {
                slidesPerView: 4,
            },
        },
        freeMode: true,
        watchSlidesProgress: true,
    });

    const swiperBanner = new Swiper('.banner-swiper', {
        slidesPerView: 1,
        spaceBetween: 30,
        centeredSlides: true,
        loop: true,
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicBullets: true,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: swiperTitle,
            slideThumbActiveClass: 'title--active',
        },
        modules: [
            SwiperAutoplay,
            SwiperNavigation,
            SwiperThumbs,
            SwiperPagination,
        ],
    });

    return {
        swiperTitle,
        swiperBanner,
    };
}
