import { Swiper } from 'swiper';

export default function (index: string) {
    const swiperProductList = new Swiper(`.product-list-swiper__${index}`, {
        slidesPerView: 5,
        spaceBetween: 10,
        breakpoints: {
            320: {
                slidesPerView: 2,
            },
            375: {
                slidesPerView: 2.5,
            },
            717: {
                slidesPerView: 3.4,
            },
            768: {
                slidesPerView: 3,
            },
            990: {
                slidesPerView: 4,
            },
            1200: {
                slidesPerView: 5,
            },
        },
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        modules: [SwiperAutoplay, SwiperNavigation],
    });

    return {
        swiperProductList,
    };
}
