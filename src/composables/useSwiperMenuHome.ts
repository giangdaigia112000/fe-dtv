import { Swiper } from 'swiper';

export default function (index: string, row?: number) {
    const swiperMenu = new Swiper(`.menu-swiper__${index}`, {
        slidesPerView: 'auto',
        grid: {
            rows: row ?? 1,
            fill: 'column',
        },
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
        modules: [SwiperAutoplay, SwiperGrid],
    });

    return {
        swiperMenu,
    };
}
