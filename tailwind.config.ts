import type { Config } from 'tailwindcss';
export default <Partial<Config>>{
    theme: {
        screens: {
            w500: '500px',
            w600: '600px',
            w700: '700px',
            w800: '800px',
            w900: '900px',
            w1024: '1024px',
            w1280: '1280px',
            w1500: '1500px',
        },
        colors: {
            dtv: '#e11b1e',
        },
    },
};
